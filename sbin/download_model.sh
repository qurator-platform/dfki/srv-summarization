#!/bin/bash 

# Use model dir from environment variable
MODEL_DIR=${SERVICE_MODEL_DIR:=./models}
CURRENT_DIR=$(pwd)

if [[ -z "$1" ]]; then
    echo "Error: Model group not set"
    exit 1
fi


if [[ -z "$2" ]]; then
    echo "Error: Model name not set"
    exit 1
fi

MODEL_GROUP=$1
MODEL_NAME=$2

function download {
    mkdir -p $MODEL_DIR/$1/$2
    cd $MODEL_DIR/$1/$2
    # Bart Tokenizer
    wget -nc https://cdn.huggingface.co/facebook/bart-large/vocab.json
    wget -nc https://cdn.huggingface.co/facebook/bart-large/tokenizer_config.json

    # Model
    wget -nc https://cdn.huggingface.co/$1/$2/config.json
    wget -nc https://cdn.huggingface.co/$1/$2/merges.txt
    wget -nc https://cdn.huggingface.co/$1/$2/pytorch_model.bin
    
    cd $CURRENT_DIR
}

download $MODEL_GROUP $MODEL_NAME