#!/bin/bash 

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# All models
. $parent_path/download_model.sh facebook bart-large-cnn
. $parent_path/download_model.sh airKlizz distilbart-12-6-multi-combine-wiki-news
. $parent_path/download_model.sh airKlizz bart-large-multi-en-wiki-news