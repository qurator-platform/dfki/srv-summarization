#!/bin/bash

FLASK_HOST=${FLASK_HOST:-0.0.0.0}
FLASK_PORT=${FLASK_PORT:-8000}

GUNICORN_LOG_LEVEL=${GUNICORN_LOG_LEVEL:-debug}

gunicorn -b $FLASK_HOST:$FLASK_PORT --log-file - --log-level $GUNICORN_LOG_LEVEL qurator_srv.app:app