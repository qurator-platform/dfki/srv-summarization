import logging.config
import os

from flask import Flask
from flask_cors import CORS

from qurator_srv import ReverseProxied
from qurator_srv.api import api_blueprint

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

log_file_path = os.path.join(BASE_DIR, 'logging.conf')
logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)
# logging.basicConfig(
#         level=logging.DEBUG,
#         format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
#         )

logger = logging.getLogger(__name__)

app = Flask(__name__)
app.config.from_object('qurator_srv.settings')
# app.logger.setLevel(logging.DEBUG)
app.wsgi_app = ReverseProxied(app.wsgi_app, script_name=app.config['SERVICE_PATH'])

# Allow AJAX requrest from cross-origins
cors = CORS(app, resources={r"/*": {"origins": "*"}})

app.logger.debug('foo  ########################')

# Add REST endpoints to app
app.register_blueprint(api_blueprint)

# Add CLI commands
# app.cli.add_command(train_model_command)


# Log settings at start
for k, v in app.config.items():
    logger.info(f'{k}: {v}')


if __name__ == '__main__':
    # Use main only for debugging. Production should use gunicorn.
    app.run(
        debug=app.config['DEBUG'],
        host=app.config['HOST'],
        port=app.config['PORT'],
    )