import logging
import os

from flask import Blueprint
from flask_restplus import Resource, apidoc
from werkzeug.exceptions import BadRequest

import qurator_srv
from qurator_srv import CustomAPI
from qurator_srv.settings import PATH, API_TITLE, API_ENDPOINT_TITLE, API_ENDPOINT_LABEL, SERVICE_MODEL_DIR

from transformers import AutoTokenizer, AutoModelWithLMHead

import time

logger = logging.getLogger(__name__)

_ALL_MODELS_NAME = [
    "airKlizz/bart-large-multi-en-wiki-news",
    "airKlizz/distilbart-12-6-multi-combine-wiki-news",
    "facebook/bart-large-cnn",
]


def load_model(model_name, _models, _tokenizers):
    logger.info(f"Loading {model_name}")
    _models[model_name] = AutoModelWithLMHead.from_pretrained(SERVICE_MODEL_DIR+"/"+model_name)
    _tokenizers[model_name] = AutoTokenizer.from_pretrained(SERVICE_MODEL_DIR+"/"+model_name)
    return _models, _tokenizers


def init_models():

    logger.info('Initialize models')
    logger.info(f'Files in model dir: {os.listdir(SERVICE_MODEL_DIR)}')

    _models = {}
    _tokenizers = {}

    for model_name in _ALL_MODELS_NAME:
        _models[model_name] = None
        _tokenizers[model_name] = None

        # Load models from disk if they exist as path
        # model_path = os.path.join(SERVICE_MODEL_DIR, model_name)
        # if os.path.exists(model_path) \
        #         and os.path.isdir(model_path) \
        #         and os.path.exists(os.path.join(model_path, 'pytorch_model.bin')):
        #     logger.info(f'Initialize model from disk: {model_name}')
        #
        #     _models, _tokenizers = load_model(model_name, _models, _tokenizers)

    return _models, _tokenizers

models, tokenizers = init_models()
MODELS = {
    "models": models,
    "tokenizers": tokenizers,
}

# Global scope
api_blueprint = Blueprint('api', __name__, url_prefix=PATH)

api = CustomAPI(
    api_blueprint,
    version=qurator_srv.__version__,
    title=API_TITLE,
    default=API_ENDPOINT_TITLE,
    default_label=API_ENDPOINT_LABEL,
)


@api.route('/models')
class ModelsResource(Resource):
    def get(self):
        """
        List available models
        """
        return _ALL_MODELS_NAME


summarization_parser = api.parser()
summarization_parser.add_argument('text', type=str, required=True, help='text to summarize')
# For details about generation parameters, see https://github.com/huggingface/transformers/blob/master/src/transformers/generation_utils.py#L113
summarization_parser.add_argument('max_length', type=int, required=False, default=400, help='max_length of the summary')
summarization_parser.add_argument('min_length', type=int, required=False, default=150, help='min_length of the summary')
summarization_parser.add_argument('length_penalty', type=float, required=False, default=2.0, help='length_penalty of the summary')
summarization_parser.add_argument('num_beams', type=int, required=False, default=4, help='num_beams of the summary')



@api.route('/models/<string:model_folder>/<string:model_name>/summarize')
class SummarizeResource(Resource):
    @api.expect(summarization_parser)
    def post(self, model_folder, model_name):
        """
        Summarize text documents.
        """
        logger.info('Request received')
        args = summarization_parser.parse_args()  # works with POST,GET,JSON

        try:

            model_name = model_folder+"/"+model_name

            models = MODELS["models"]
            tokenizers = MODELS["tokenizers"]

            if model_name not in models:
                raise BadRequest(f'Invalid model name: {model_name}. Please select one of: {list(models.keys())}')

            # Load model and tokenizer if needed
            if models[model_name] == None:
                models, tokenizers = load_model(model_name, models, tokenizers)
                MODELS["models"] = models
                MODELS["tokenizers"] = tokenizers

                logger.info('Model and tokenizer loaded')

            start_time = time.time()

            inputs = tokenizers[model_name](args["text"], max_length=tokenizers[model_name].max_len, truncation=True, return_tensors="pt")
            logger.info('Input tokenized')

            args.pop("text")
            outputs = models[model_name].generate(
                input_ids=inputs['input_ids'], 
                attention_mask=inputs['attention_mask'], 
                **args,
            )
            logger.info('Model forward pass completed')

            summary = tokenizers[model_name].decode(outputs[0], skip_special_tokens=True, clean_up_tokenization_spaces=False)
            logger.info('Model output decoded')

            end_time = time.time()

            return {
                "summary": summary,
                "inference_time": end_time - start_time,
            }

        except (KeyError, ValueError) as e:
            raise BadRequest(f'Summarization failed: {e}')


@api.documentation
def swagger_ui():
    return apidoc.ui_for(api)

