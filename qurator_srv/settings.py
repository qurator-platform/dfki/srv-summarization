import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SERVICE_PATH = os.getenv('ELG_HTTP_PATH_PREFIX', '/srv/summarization')
SERVICE_MODEL_DIR = os.getenv('SERVICE_MODEL_DIR', os.path.join(BASE_DIR, 'models'))

API_TITLE = os.getenv('FLASK_API_TITLE', 'Summarization API')
API_ENDPOINT_TITLE = os.getenv('FLASK_API_ENDPOINT_TITLE', 'Summarization')
API_ENDPOINT_LABEL = os.getenv('FLASK_API_ENDPOINT_LABEL', '')


PATH = os.getenv('FLASK_PATH', '')
DEBUG = os.getenv('FLASK_DEBUG', False)
HOST = os.getenv('FLASK_HOST', 'localhost')
PORT = os.getenv('FLASK_PORT', 5000)
