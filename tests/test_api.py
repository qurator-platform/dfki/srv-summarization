from unittest import TestCase

from qurator_srv.app import app

class ApiTest(TestCase):
    def setUp(self) -> None:
        app.config['TESTING'] = True
        app.config['SERVICE_MODEL_DIR'] = '/foo'

    def test_api(self):
        # root
        res = app.test_client().get('/')
        self.assertEqual(res.status_code, 200)

    def test_api_models(self):
        # models
        res = app.test_client().get('/models')
        self.assertEqual(res.status_code, 200)

    def test_summarize(self):
        model = 'airKlizz/distilbart-12-6-multi-combine-wiki-news'
        text_to_summarize = """The following is a release by the United States Geological Survey, National Earthquake Information Center: An earthquake occurred 115 km (75 miles) S of Isangel, Tanna, Vanuatu 1830 km (1140 miles) ENE of BRISBANE, Queensland, Australia at 6:40 PM MDT, Mar 24, 2007 (Mar 25 at 11:40 AM local time in Vanuatu). The magnitude and location may be revised when additional data and further analysis results are available. There have been no reports of damage.
        Felt Reports
        Felt (III) at Vila. Felt (III) at Mont-Dore, New Caledonia. Also felt at Noumea and Yate, New Caledonia.
        Tsunami Information|||At least one person was killed and 150 were reportedly injured by the tremor, which was felt in the capital, Tokyo.
        A tsunami warning was issued for a short time in Ishikawa prefecture, with swell of up to 50cm reported.
        Meanwhile, two quakes - measured at 7.2 and 6.0 by the US - hit near Vanuatu in the southern Pacific Ocean. There were no reports of damage or casualties.
        The larger quake struck at 1140 (0040 GMT Sunday), and was followed 28 minutes later by the magnitude 6.0 quake.
        Both were centred 335km (210 miles) south-east of the capital, Port Vila.
        Police said they were checking remote islands where the quake was closer to land areas.
        Seismically active
        The Japan earthquake struck at 0942 (0042 GMT). Its epicentre was 300km (200 miles) north-west of Tokyo, Japan's meteorological agency said.
        Television pictures showed buildings in the Ishikawa prefecture shaking violently for about 30 seconds and Japan's public broadcaster NHK reported that several buildings had collapsed.
        The BBC's Chris Hogg in Hong Kong says there are also reports of landslides, a ruptured water main and roads being buckled by the jolt.
        A 52-year-old woman died in Wajima, a resort and fishing town on the western side of the peninsula, after being trapped under a stone lantern that toppled in her garden, reports said.
        "I wasn't able to stand at all, it was really terrible", one woman in Wajima told NHK.
        Many of those injured were hit by falling debris or broken glass and are being treated in hospital, with some said to be badly hurt.
        Government troops have been sent to the area to assist with the clear up and warnings have been issued of further aftershocks in the affected region.
        Wooden houses were badly damaged by the Japan quake
        Earthquakes are common in Japan, one of the world's most seismically active areas.
        In October 2004, an earthquake with a magnitude of 6.8 struck the Niigata region in northern Japan, killing 65 people and injuring more than 3,000.
        In 1995 a magnitude 7.2 tremor killed more than 6,400 people in the city of Kobe.|||Powerful quake rocks Japan Associated Press TOKYO  A powerful earthquake rocked northern Japan on Sunday, triggering small tsunamis and killing at least one person. The magnitude 7.1 quake struck shortly before 10 a.m. off the north coast of Ishikawa prefecture, Japan's Meteorological Agency said. The agency issued a tsunami warning urging people near the sea to move to higher land. A small tsunami measuring 6 inches hit shore around 10:18 a.
        """

        res = app.test_client().post(f'/models/{model}/summarize', data=dict(
            text=text_to_summarize,
            num_beams=1
        ))
        data = res.data
        json = res.json

        print("Test summarize result:")
        print(res)
        print(data)
        print(json)

        self.assertEqual(res.status_code, 200)
        self.assertTrue("summary" in json.keys())


