# Summarization

Demo at: https://demo.qurator.ai/pub/srv-summarization


## Datasets

- WikinewsSum
- CNN DailyMail

## Available models

- airKlizz/bart-large-multi-en-wiki-news
- airKlizz/distilbart-12-6-multi-combine-wiki-news
- facebook/bart-large-cnn

## Data format

- text

## Install

```bash
# create conda env
conda create -n srv-summarization python=3.7

# install deps
pip install -r requirements.txt

# activate Flask CLI
export FLASK_APP=qurator_srv.app
```

## Docker

```bash
# build
docker build -t srv-summarization .

# start API endpoint at http://localhost:8000
docker run -it -p 8000:8000 -v $(pwd)/models:/app/models srv-summarization

# download models
docker run -it -p 8000:8000 -v $(pwd)/models:/app/models srv-summarization /app/sbin/download_model.sh airKlizz distilbart-12-6-multi-combine-wiki-news

# run unit tests
docker run -it  -v $(pwd)/models:/app/models srv-summarization nosetests tests.test_api
```


## Deploy models

Use [kubectl](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) to access deployment infrastructure.

```bash
export NS=qsrv
export POD=...

# View config
kubectl config view

# List all services in the namespace
kubectl get services

# List all pods 
kubectl get pods              

# View logs
kubectl logs $POD

# Attach
kubectl attach $POD -i   
kubectl exec -ti -n $NS $POD bash

# Copy models 
kubectl -n $NS cp models/* $POD:/qurator/data/srv-summarization/models/

# Download models
kubectl exec -ti -n $NS $POD /app/sbin/download_all_models.sh

# List models on persistent volume
kubectl exec -ti -n $NS $POD "ls -l /qurator/data/srv-summarization/models/"

# Redeploy service
kubectl -n $NS delete pod $POD
```

## License

Internal use only
