FROM python:3.7
LABEL maintainer="malte.ostendorff@dfki.de"

ENV FLASK_DEBUG=-1
ENV FLASK_PATH=""
ENV FLASK_HOST="0.0.0.0"
ENV FLASK_PORT=8000

ENV ELG_HTTP_PATH_PREFIX=/srv/summarization
ENV SERVICE_MODEL_DIR=/app/models

WORKDIR /app

ADD ./requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

ADD . /app
RUN mkdir -p /app/logs
RUN mkdir -p /app/models

EXPOSE 8000

# Run the green unicorn
CMD gunicorn -w 4 -b 0.0.0.0:8000 --timeout 120 --log-level debug --log-file - qurator_srv.app:app
